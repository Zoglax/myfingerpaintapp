package com.example.myfingerpaint.commonentities;

public class DataTransferClass
{
    public static int LineWidth;
    public static int LineColorR;
    public static int LineColorB;
    public static int LineColorG;

    public enum ActivityType
    {
        ChooseColorActivity,
        ChooseWidthActivity;
    }

    public static ActivityType LastActivity;
    public static boolean IsCanceled;
}
