package com.example.myfingerpaint;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.myfingerpaint.commonentities.DataTransferClass;

import java.util.Random;

public class ChooseWidthActivity extends AppCompatActivity {

    TextView textViewWidth;
    SeekBar seekBarWidth;
    Button buttonSaveWidthAndExit, buttonCancelWidth, buttonRandomWidth;

    TextView textViewNewWidth;

    Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_width);

        random = new Random();

        textViewWidth = findViewById(R.id.textViewWidth);

        textViewNewWidth = findViewById(R.id.textViewNewWidth);

        seekBarWidth = findViewById(R.id.seekBarWidth);

        buttonSaveWidthAndExit = findViewById(R.id.buttonSaveWidthAndExit);
        buttonCancelWidth = findViewById(R.id.buttonCancelWidth);
        buttonRandomWidth = findViewById(R.id.buttonRandomWidth);

        seekBarWidth.setOnSeekBarChangeListener(seekBarWidthOnSeek);

        buttonSaveWidthAndExit.setOnClickListener(buttonSaveWidthAndExitOnClick);
        buttonCancelWidth.setOnClickListener(buttonCancelWidthOnClick);
        buttonRandomWidth.setOnClickListener(buttonRandomWidthOnClick);
    }

    private void ShowResultWidth()
    {
        int width = seekBarWidth.getProgress();

        textViewNewWidth.setWidth(width);
    }

    SeekBar.OnSeekBarChangeListener seekBarWidthOnSeek = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            ShowResultWidth();

            textViewWidth.setText("Width: "+seekBarWidth.getProgress());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    View.OnClickListener buttonSaveWidthAndExitOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.LastActivity = DataTransferClass.ActivityType.ChooseWidthActivity;
            DataTransferClass.IsCanceled = false;

            DataTransferClass.LineWidth = seekBarWidth.getProgress();

            finish();
        }
    };
    View.OnClickListener buttonCancelWidthOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.LastActivity = DataTransferClass.ActivityType.ChooseWidthActivity;
            DataTransferClass.IsCanceled = true;
            finish();
        }
    };

    View.OnClickListener buttonRandomWidthOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            int width = random.nextInt(150);

            seekBarWidth.setProgress(width);

            textViewWidth.setText("Red: "+seekBarWidth.getProgress());

            ShowResultWidth();
        }
    };
}
