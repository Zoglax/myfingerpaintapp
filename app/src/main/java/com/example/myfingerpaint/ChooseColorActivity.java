package com.example.myfingerpaint;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.myfingerpaint.commonentities.DataTransferClass;

import java.util.Random;

public class ChooseColorActivity extends AppCompatActivity {

    TextView textViewRed, textViewGreen, textViewBlue;
    SeekBar seekBarRed, seekBarGreen, seekBarBlue;
    Button buttonSaveColorAndExit, buttonCancelColor, buttonRandomColor;

    TextView textViewNewColor;

    Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_color);

        random = new Random();

        textViewRed = findViewById(R.id.textViewRed);
        textViewGreen = findViewById(R.id.textViewGreen);
        textViewBlue = findViewById(R.id.textViewBlue);

        textViewNewColor = findViewById(R.id.textViewNewColor);

        seekBarRed = findViewById(R.id.seekBarRed);
        seekBarGreen = findViewById(R.id.seekBarGreen);
        seekBarBlue = findViewById(R.id.seekBarBlue);

        buttonSaveColorAndExit = findViewById(R.id.buttonSaveColorAndExit);
        buttonCancelColor = findViewById(R.id.buttonCancelColor);
        buttonRandomColor = findViewById(R.id.buttonRandomColor);

        seekBarRed.setOnSeekBarChangeListener(seekBarRedOnSeek);
        seekBarGreen.setOnSeekBarChangeListener(seekBarGreenOnSeek);
        seekBarBlue.setOnSeekBarChangeListener(seekBarBlueOnSeek);

        buttonSaveColorAndExit.setOnClickListener(buttonSaveColorAndExitOnClick);
        buttonCancelColor.setOnClickListener(buttonCancelColorOnClick);
        buttonRandomColor.setOnClickListener(buttonRandomColorOnClick);
    }

    private void ShowResultColor()
    {
        int r = seekBarRed.getProgress();
        int g = seekBarGreen.getProgress();
        int b = seekBarBlue.getProgress();

        textViewNewColor.setBackgroundColor(Color.argb(255,r,g,b));
    }

    SeekBar.OnSeekBarChangeListener seekBarRedOnSeek = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            ShowResultColor();

            textViewRed.setText("Red: "+seekBarRed.getProgress());
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
    SeekBar.OnSeekBarChangeListener seekBarGreenOnSeek = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            ShowResultColor();

            textViewGreen.setText("Green: "+seekBarGreen.getProgress());
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
    SeekBar.OnSeekBarChangeListener seekBarBlueOnSeek = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            ShowResultColor();

            textViewBlue.setText("Blue: "+seekBarBlue.getProgress());
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    View.OnClickListener buttonSaveColorAndExitOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.LastActivity = DataTransferClass.ActivityType.ChooseColorActivity;
            DataTransferClass.IsCanceled = false;

            DataTransferClass.LineColorR = seekBarRed.getProgress();
            DataTransferClass.LineColorG = seekBarGreen.getProgress();
            DataTransferClass.LineColorB = seekBarBlue.getProgress();

            finish();
        }
    };
    View.OnClickListener buttonCancelColorOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.LastActivity = DataTransferClass.ActivityType.ChooseColorActivity;
            DataTransferClass.IsCanceled = true;
            finish();
        }
    };

    View.OnClickListener buttonRandomColorOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            int r = random.nextInt(255);
            int g = random.nextInt(255);
            int b = random.nextInt(255);

            seekBarRed.setProgress(r);
            seekBarGreen.setProgress(g);
            seekBarBlue.setProgress(b);

            textViewRed.setText("Red: "+seekBarRed.getProgress());
            textViewGreen.setText("Red: "+seekBarGreen.getProgress());
            textViewBlue.setText("Red: "+seekBarBlue.getProgress());

            ShowResultColor();
        }
    };
}
