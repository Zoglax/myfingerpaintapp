package com.example.myfingerpaint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.media.Image;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.example.myfingerpaint.commonentities.DataTransferClass;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    MyCanvas myCanvas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myCanvas = new MyCanvas(getApplicationContext());
        setContentView(myCanvas);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_canvas_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.itemClearCanvas:
                myCanvas.clearCanvas();
                return true;
            case R.id.itemChooseColor:
                Intent intentColor = new Intent(getApplicationContext(), ChooseColorActivity.class);
                startActivityForResult(intentColor,1);
                return true;

            case R.id.itemChooseWidth:
                Intent intentWidth = new Intent(getApplicationContext(), ChooseWidthActivity.class);
                startActivityForResult(intentWidth,1);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (DataTransferClass.IsCanceled==true)
        {
            return;
        }

        switch (DataTransferClass.LastActivity)
        {
            case ChooseColorActivity:
                myCanvas.setLineColor(
                        DataTransferClass.LineColorR,
                        DataTransferClass.LineColorG,
                        DataTransferClass.LineColorB);
                break;
            case ChooseWidthActivity:
                myCanvas.setLineWidth(DataTransferClass.LineWidth);
                break;
        }
    }

    public class MyCanvas extends View{

        class Line
        {
            public Paint paint;
            public Path path;

            public Line()
            {
                path = new Path();

                paint = new Paint();
                paint.setARGB(255,0,255,0);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeJoin(Paint.Join.ROUND);
                paint.setStrokeCap(Paint.Cap.ROUND);
                paint.setAntiAlias(true);
                paint.setStrokeWidth(70);
            }
        }

        ArrayList<Line> lines;
        int lastR, lastG, lastB;
        int lastWidth;

        private Line getLastLine()
        {
            return lines.get(lines.size()-1);
        }

        public void clearCanvas()
        {
            lines.clear();
            invalidate();
        }

        public void setLineColor(int r, int g, int b)
        {
            lines.add(new Line());

            Line lastLine = getLastLine();

            lastLine.paint.setARGB(255,r,g,b);
            lastLine.paint.setStrokeWidth(lastWidth);

            lastR = r;
            lastG = g;
            lastB = b;
        }

        public void setLineWidth(int width)
        {
            lines.add(new Line());

            Line lastLine = getLastLine();

            lastLine.paint.setStrokeWidth(width);
            lastLine.paint.setARGB(255,lastR, lastG, lastB);

            lastWidth = width;
        }

        public MyCanvas(Context context)
        {
            super(context);

            lastR = 0;
            lastG = 255;
            lastB = 0;

            lastWidth = 70;

            lines = new ArrayList<>();
            lines.add(new Line());
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);

            for (int i = 0; i < lines.size(); i++)
            {
                canvas.drawPath(lines.get(i).path, lines.get(i).paint);
            }

        }

        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            float x = event.getX();
            float y = event.getY();

            Line lastLine = getLastLine();

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    lastLine.path.moveTo(x,y);
                    lastLine.path.lineTo(x,y);
                    invalidate();
                    break;

                case MotionEvent.ACTION_MOVE:
                    lastLine.path.lineTo(x,y);
                    invalidate();
                    break;
            }

            return true;
        }

    }
}
